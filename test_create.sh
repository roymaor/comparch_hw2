#!/bin/bash
dos2unix ./*/*
make
counter=1


echo "---------Our Semester Tests---------"


while (( $counter <=  6))
do
	echo -n "./bp_main ./os_tests/example${counter}_in.txt > ./os_our_results/example${counter}_out.txt"
        ./bp_main ./os_tests/example${counter}_in.txt > ./os_our_results/example${counter}_out.txt
		if diff ./os_results/example${counter}_out.txt ./os_our_results/example${counter}_out.txt >/dev/null ; then
			  echo "---------->SUCCESS"
		else
			  echo "---------->FAIL"
		fi

	((counter++))
done 

: '

counter=1

echo "---------Last Semester Tests---------"


while (( $counter <=  4))
do
	echo -n "./sim_main ./ls_tests/example${counter}.img 43  > ./ls_our_results/out${counter}.txt"
        ./sim_main ./ls_tests/example${counter}.img 43  > ./ls_our_results/out${counter}.txt
		if diff ./ls_results/example${counter}.img ./ls_our_results/out${counter}.txt >/dev/null ; then
			  echo "---------->SUCCESS"
		else
			  echo "---------->FAIL"
		fi

        echo -n "./sim_main ./ls_tests/example${counter}.img 43 -s  > ./ls_our_results/s_out${counter}.txt"
        ./sim_main ./ls_tests/example${counter}.img 43 -s  > ./ls_our_results/s_out${counter}.txt
		if diff ./ls_results/example${counter}-s.img ./ls_our_results/s_out${counter}.txt >/dev/null ; then
			  echo "----->SUCCESS"
		else
			  echo "----->FAIL"
		fi

        echo -n "./sim_main ./ls_tests/example${counter}.img 43 -f  > ./ls_our_results/f_out${counter}.txt"
        ./sim_main ./ls_tests/example${counter}.img 43 -f  > ./ls_our_results/f_out${counter}.txt
		if diff ./ls_results/example${counter}-f.img ./ls_our_results/f_out${counter}.txt >/dev/null ; then
			  echo "----->SUCCESS"
		else
			  echo "----->FAIL"
		fi
	((counter++))
done 

'

counter=1

echo "---------Facebook Tests---------"

while (( $counter <=  20))
do
	echo -n "./bp_main ./os_tests/ex${counter}_in.txt > ./os_our_results/ex${counter}_out.txt"
        ./bp_main ./os_tests/ex${counter}_in.txt > ./os_our_results/ex${counter}_out.txt
		if diff ./os_results/ex${counter}_out.txt ./os_our_results/ex${counter}_out.txt >/dev/null ; then
			  echo "---------->SUCCESS"
		else
			  echo "---------->FAIL"
		fi

	((counter++))
done 


