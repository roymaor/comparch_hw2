/* 046267 Computer Architecture - Spring 2016 - HW #2 */
/* This file should hold your implementation of the predictor simulator */

#include "bp_api.h"
#include <vector>
#include <ctgmath>
#include <iostream>
#include <bitset>

#define TARGET_SIZE 30

using namespace std;


typedef enum {
	SNT = 0, WNT, WT, ST
} State;



unsigned int getFirstSetBitPos(int n)
{
    return log2(n&-n)+1;
}

class StateMachine{
private:
    int current_state;

public:
	StateMachine(){
		current_state = WNT;
	}

    State getState() {return (State)current_state;}

    void resetState(){ current_state = WNT;}

	void stateUp(){
			if (current_state != ST)
				current_state++;
	}

	void stateDown() {
        if (current_state != SNT)
            current_state-- ;
    }

};


class counterArray {
private:
	vector<vector<StateMachine>> table;

public:
	counterArray(int sizeH,  int btbSize) {
		table.assign((unsigned long)btbSize, vector<StateMachine>((unsigned)pow(2,sizeH)));
	}

    StateMachine getState(int entryNum, int hist) { return table[entryNum][hist];}

    void resetEntryToWNT(int entryNum){
        int size = pow(2,table[entryNum].size());
	    for (int i=0; i<size; i++)
            table[entryNum][i].resetState();
	}

    void updateStateTaken(int entryNum, int hist){
        (table[entryNum][hist]).stateUp();
	}
    void updateStateNotTaken(int entryNum, int hist) {
	    (table[entryNum][hist]).stateDown();
	}


};


class Entry{
private:
    int dst; // cached pc
    unsigned int history;
    unsigned int tag;
	int id;

public:

    static int count;

	Entry()
    {
		history = 0;
        tag =  0;
        dst = 0;
        id = count;
        count++;
	}

    ~Entry() {
        count--;
    }


    unsigned int getHistory() {return history;}
	void setHistory(unsigned int newHist)
    {
        //if (!valid) return;
        history = newHist;
	}
    int getTag(){return tag;}
    void setTag(unsigned int newTag)
    {
	    //if (!valid) return;
	    tag = newTag;
	}
    int getDst(){return dst;}
    int getEntryNum(){return id;}
    void setDst(int newDst)
    {
        //if (!valid) return;
            dst = newDst;
    }
};


class BTB{
private:

    vector<Entry*> table;
	unsigned int size_btb;
    int mask; // Used to extract bits [2:log2(size_btb)] from PC for fast lookup in BTB table - refer to BTB organiztion in HW sheet
    int maskHist;
    int shareLG; //Only relevant for Global Table
    unsigned int size_history;
    unsigned int size_tag;
	bool isGHist;
	bool isGTable;



public:
	BTB(unsigned int sizeBTB,unsigned int sizeHis,unsigned int sizeTag, int shared, bool GlobalHist, bool GlobalTable){
        size_btb = sizeBTB;
        mask = ((1 << (getFirstSetBitPos(sizeBTB)-1)) - 1);
        //cout << sizeBTB << " " << bitset<32>(mask) <<endl;

        size_history = sizeHis;
        maskHist = ((1 << sizeHis) - 1);
        //cout << bitset<32>(maskHist) <<endl;

		size_tag = sizeTag;

        table.resize(sizeBTB);
		for(int i=0; i<(int)sizeBTB; i++){
			table[i] = new Entry();
		}
		shareLG = shared;

        isGTable = GlobalTable;
        isGHist = GlobalHist;
	}

	~BTB()
    {
        for(int i=0; i<(int)size_btb; i++)
            delete table[i];
    }

    vector<Entry*>* getTable() { return &table;}
    unsigned int getBTBSize() {return size_btb;}
    int getLookupMask() {return mask;}
    int getHistMask() {return maskHist;}
	int getShareLG() {return shareLG;}
    unsigned int getTagSize() {return size_tag;}
    unsigned int getHistSize() {return size_history;}
	bool isGlobalTable() {return isGTable;}
	bool isGlobalHist() {return isGHist;}

};


int Entry::count =0;
BTB* btb;
counterArray* ca;
SIM_stats stat;

void printStatus()
{
    vector<Entry*>* btbTable = btb->getTable();
    //int hist_size = btb->getHistSize();
    cout << endl << "History: " << endl;
    for( int i= 0; i<(int)btb->getBTBSize(); i++)
    {
        cout << i << " TAG: " << (*btbTable)[i]->getTag() << " " << bitset<32>((*btbTable)[i]->getHistory())<< " DST: " << (*btbTable)[i]->getDst() << endl;
    }

    cout << endl << endl << "Counter Array: " << endl;
    for( int i= 0; i<(int)btb->getBTBSize(); i++)
    {
        cout << i << " : ";
        for( int j= 0; j<pow(2,btb->getHistSize()); j++) {
            int dude = (int) (ca->getState(i,j)).getState();
            cout <<  dude << " ";
        }
        cout << endl;
    }
}

unsigned int TagPc(unsigned int pc)
{
  return (pc & ((1 << btb->getTagSize()) - 1) << 2) >>2;
}

int BP_init(unsigned btbSize, unsigned historySize, unsigned tagSize,
             bool isGlobalHist, bool isGlobalTable, int Shared){
	if(!(btbSize == 1 || btbSize == 2 || btbSize == 4 ||
			btbSize == 8 || btbSize == 16 || btbSize == 32)) { //2^n (1-32)
        return -1;
    }

	if(!(historySize > 0 && historySize < 9)) //1-8
		return -1;
	if(!(tagSize >=0 && tagSize < 31)) // 0-30
		return -1;

	btb = new BTB(btbSize, historySize, tagSize, Shared, isGlobalHist, isGlobalTable);
    ca = new counterArray(historySize, btbSize);


    //initiate stat;
    stat.br_num = 0;
    stat.flush_num = 0;
    stat.size = 0;

	if(isGlobalHist && isGlobalTable)
		stat.size = btbSize*(tagSize+TARGET_SIZE) + historySize + 2*pow(2,historySize);
	else if(isGlobalHist && !isGlobalTable)
		stat.size = btbSize*(tagSize+TARGET_SIZE + 2*pow(2,historySize)) + historySize;
	else if(!isGlobalHist && isGlobalTable)
		stat.size = btbSize*(tagSize+TARGET_SIZE + historySize) + 2*pow(2,historySize);
	else //both local
		stat.size = btbSize*(tagSize+TARGET_SIZE + historySize + 2*pow(2,historySize));


    //cout << "btb->getLookupMask() : " << bitset<32>( btb->getLookupMask()) <<endl;// << " pc: " << bitset<32>(pc>>2) <<endl;
    //printStatus();

	return 0;
}

unsigned int getEntryHistory(Entry* curr_ent) {
    //unsigned int newHist;
    vector<Entry*>* btbTable = btb->getTable();
    if (btb->isGlobalHist())
        //if global use history[0]
        return (*btbTable)[0]->getHistory() & btb->getHistMask();
    else
        return curr_ent->getHistory() & btb->getHistMask();

}

void setLocalEntryHistory(Entry* curr_ent, unsigned int newHist) {
    vector<Entry*>* btbTable = btb->getTable();
    newHist &= btb->getHistMask();
    if (btb->isGlobalHist())
        (*btbTable)[0]->setHistory(newHist);
    else
        curr_ent->setHistory(newHist);
}

int LGShareModifier(unsigned int pc, unsigned int Hist) {
    int  newHist = Hist;

    if (btb->getShareLG() == 1) //need to search counter array -> take number of history bits from 2nd bit of pc and xor it with tag
        newHist = Hist xor ((pc>>2) & btb->getHistMask());

    else if (btb->getShareLG() == 2) //need to search counter array -> take number of history bits from 16th bit of pc and xor it with tag
        newHist = Hist xor ((pc>>16) & btb->getHistMask());

    return newHist;
}

bool BP_predict(uint32_t pc, uint32_t *dst) {
    // PARAMETERS SETUP
    unsigned int tag = TagPc(pc);
    vector<Entry*>* btbTable = btb->getTable();
    stat.br_num++;
    *dst = pc+4; // default Not taken
    unsigned int isolatedPCbits = (pc>>2) & btb->getLookupMask(); //extract the needed bits from PC to lookup in BTB;
    Entry* curr_ent =(*btbTable)[isolatedPCbits]; // GET BTB ENTRY

    unsigned int currHist = getEntryHistory(curr_ent);
    //cout << "ENTRY USED: " << curr_ent->getEntryNum() << " " <<endl;
    //cout << "TAGS COMPARISION: " << curr_ent->getTag() << " " << tag <<endl;
    if (curr_ent->getTag() == (int)tag) {
        if (btb->isGlobalTable()) {
            //cases For shareLG
            currHist = LGShareModifier(pc, currHist);

            if (ca->getState(0, currHist).getState() >= WT) {
                //cout << "TAKEN!"<< endl;
                *dst = curr_ent->getDst();
                return true; //TAKEN
            }
        } else {
            if (ca->getState(curr_ent->getEntryNum(), currHist).getState() >= WT) {
                *dst = curr_ent->getDst();
                return true; //TAKEN
            }
        }
    }
	return false; // NOT TAKEN as default
}

/*
    1. Make tag Entry exist
    2. Update counter array state in old history address
    3. Update history in btb
 */
void BP_update(uint32_t pc, uint32_t targetPc, bool taken, uint32_t pred_dst) {
    // PARAMETERS SETUP
    unsigned int tag = TagPc(pc);
    vector<Entry*>* btbTable = btb->getTable();
    unsigned int isolatedPCbits = (pc>>2) & btb->getLookupMask(); //extract the needed bits from PC to lookup in BTB;
    Entry* curr_ent =(*btbTable)[isolatedPCbits]; // GET BTB ENTRY
    int currHist = getEntryHistory(curr_ent); // GET HISTORY
    int newHist =0;



    //1. Make tag Entry exist
    if (curr_ent->getTag() != (int)tag)
    {   // Tag was not exist -> handle as WNT, update BTB
        curr_ent->setTag(tag);
        curr_ent->setDst(targetPc);

        if (!btb->isGlobalTable())
            ca->resetEntryToWNT(curr_ent->getEntryNum());
        if (!btb->isGlobalHist())
            currHist = 0; // Will make History reset in Stage 2
    }


    if (! ( ((pred_dst == targetPc) && taken) || ((pred_dst == pc + 4) && !taken) ) ) { //wrong prediction
        stat.flush_num++;
    }

    //2. Update counter array state in old history address
    if (taken) {
        newHist = ((currHist << 1) + 1) & btb->getHistMask();
        currHist = LGShareModifier(pc, currHist);

        if (btb->isGlobalTable())
            ca->updateStateTaken(0, currHist);
        else
            ca->updateStateTaken(curr_ent->getEntryNum(), currHist);

    } else {
        newHist = (currHist << 1) & btb->getHistMask();
        currHist = LGShareModifier(pc, currHist);

        if (btb->isGlobalTable())
            ca->updateStateNotTaken(0, currHist);
        else
            ca->updateStateNotTaken(curr_ent->getEntryNum(), currHist);

    }


    //3. Update history in btb
    setLocalEntryHistory(curr_ent,newHist);
    //printStatus();
}


void BP_GetStats(SIM_stats *curStats) {
    curStats->size=stat.size;
    curStats->flush_num= stat.flush_num;
    curStats->br_num = stat.br_num;
}

